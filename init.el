
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)
(add-to-list 'load-path "~/.emacs.d/pkg/")
(add-to-list 'load-path "~/.emacs.d/el-get/el-get")


(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.githubusercontent.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

(add-to-list 'el-get-recipe-path "~/.emacs.d/el-get-user/recipes")
(el-get 'sync)

(setq ergoemacs-theme nil) ;; Uses Standard Ergoemacs keyboard theme
(setq ergoemacs-keyboard-layout "us") ;; Assumes QWERTY keyboard layout
(ergoemacs-mode 1)

 (add-hook 'markdown-mode-hook
            (lambda ()
              (when buffer-file-name
                (add-hook 'after-save-hook
                          'check-parens
                          nil t))))

;; Tabbar
(require 'tabbar)
;; Tabbar settings
(set-face-attribute
 'tabbar-default nil
 :background "gray20"
 :foreground "gray20"
 :box '(:line-width 1 :color "gray20" :style nil))
(set-face-attribute
 'tabbar-unselected nil
 :background "gray30"
 :foreground "white"
 :box '(:line-width 5 :color "gray30" :style nil))
(set-face-attribute
 'tabbar-selected nil
 :background "gray75"
 :foreground "black"
 :box '(:line-width 5 :color "gray75" :style nil))
(set-face-attribute
 'tabbar-highlight nil
 :background "white"
 :foreground "black"
 :underline nil
 :box '(:line-width 5 :color "white" :style nil))
(set-face-attribute
 'tabbar-button nil
 :box '(:line-width 1 :color "gray20" :style nil))
(set-face-attribute
 'tabbar-separator nil
 :background "gray20"
 :height 0.6)

;; Change padding of the tabs
;; we also need to set separator to avoid overlapping tabs by highlighted tabs
(custom-set-variables
 '(tabbar-separator (quote (0.5))))
;; adding spaces
(defun tabbar-buffer-tab-label (tab)
  "Return a label for TAB.
That is, a string used to represent it on the tab bar."
  (let ((label  (if tabbar--buffer-show-groups
                    (format "[%s]  " (tabbar-tab-tabset tab))
                  (format "%s  " (tabbar-tab-value tab)))))
    ;; Unless the tab bar auto scrolls to keep the selected tab
    ;; visible, shorten the tab label to keep as many tabs as possible
    ;; in the visible area of the tab bar.
    (if tabbar-auto-scroll-flag
        label
      (tabbar-shorten
       label (max 1 (/ (window-width)
                       (length (tabbar-view
                                (tabbar-current-tabset)))))))))

(tabbar-mode 1)

;; Dockerfile-Mode
(require 'dockerfile-mode)
(add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode))

;; Auto-complete
(ac-config-default)
(global-auto-complete-mode t)
(define-key ac-completing-map "\M-i" 'ac-previous)
(define-key ac-completing-map "\M-k" 'ac-next)

;; comment-region
;; (global-set-key (kbd "C-/") 'comment-region)
;; (global-set-key (kbd "C-/") 'uncomment-region)			


;; Paren Mode
(show-paren-mode 1)
(require 'paren)
    (set-face-background 'show-paren-match (face-background 'default))
    (set-face-foreground 'show-paren-match "#def")
    (set-face-attribute 'show-paren-match nil :weight 'extra-bold)

;; Yaml
(add-hook 'yaml-mode-hook
        (lambda ()
            (define-key yaml-mode-map "\C-m" 'newline-and-indent)))

;; Linum
(require 'linum+)
(setq linum-format "%d ")
(global-linum-mode 1)

;; NeoTree
;;(require 'neotree)
;;(global-set-key [f8] 'neotree-toggle)

;; Disable Mode-line
(setq-default mode-line-format nil) 

;; Enable Powerline
(require 'powerline)
(powerline-default-theme)
(setq powerline-color1 "grey22")
(setq powerline-color2 "grey40")

	     
;; Python Mode
(defun my-python-hooks()
  (interactive)
  (setq tab-width 4
	python-indent 4
	python-shell-interpreter "ipython3"
	python-shell-interpreter-args "-i"))

;; sr-speedbar
(require 'sr-speedbar)
(global-set-key [f9] 'sr-speedbar-toggle)

;; Themes
(require 'color-theme-sanityinc-tomorrow)


;; disable toolbar
(tool-bar-mode -1)
(menu-bar-mode -1)

(setq make-backup-files nil) ; stop creating backup~ files

;; Emmet-mode
(add-to-list 'load-path "~/emacs.d/el-get/emmet-mode")
(require 'emmet-mode)
(add-hook 'sgml-mode-hook 'emmet-mode) ;; Auto-start on any markup modes
(add-hook 'css-mode-hook  'emmet-mode) ;; enable Emmet's css abbreviation.
(setq emmet-expand-jsx-className? t) ;; default nil

;; Autopair
(add-to-list 'load-path "~/emacs.d/el-get/autopair")
(require 'autopair)
(autopair-global-mode)
(add-hook 'python-mode-hook
          #'(lambda ()
              (setq autopair-handle-action-fns
                    (list #'autopair-default-handle-action
                          #'autopair-python-triple-quote-action))))

(add-hook 'emacs-lisp-mode-hook
          #'(lambda ()
              (push '(?` . ?')
                    (getf autopair-extra-pairs :comment))
              (push '(?` . ?')
                    (getf autopair-extra-pairs :string))))


;; Yasnippet
(add-to-list 'load-path
              "~/.emacs.d/el-get/yasnippet")
(require 'yasnippet)
;;(require 'yasnippet-snippets)
(define-key yas-minor-mode-map (kbd "<tab>") nil)
(yas-global-mode 1)

;; warnings
;;(setq warning-minimum-level :emergency)

;; cursor setting
(setq-default cursor-type '(bar . 2))

(set-cursor-color "#ff0800") ;; red

;; line highlitghting

(global-hl-line-mode 1)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (sanityinc-tomorrow-night)))
 '(custom-safe-themes
   (quote
    ("06f0b439b62164c6f8f84fdda32b62fb50b6d00e8b01c2208e55543a6337433a" default))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )



